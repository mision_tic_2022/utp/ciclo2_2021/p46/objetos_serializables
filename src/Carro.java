import java.io.Serializable;

public class Carro implements Serializable{
    /**
     * Atributos
     */
    private String color;
    private String fabricante;
    private int caballos_fuerza;
    //Método constructor
    public Carro(String color, String fabricante, int caballos_fuerza){
        this.color = color;
        this.fabricante = fabricante;
        this.caballos_fuerza = caballos_fuerza;
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getFabricante() {
        return fabricante;
    }
    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }
    public int getCaballos_fuerza() {
        return caballos_fuerza;
    }
    public void setCaballos_fuerza(int caballos_fuerza) {
        this.caballos_fuerza = caballos_fuerza;
    }


}
