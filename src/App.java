
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class App {
    public static void main(String[] args) throws Exception {
        // crear_carros("./carros.txt");
        cargar_carros("./carros.txt");
    }

    public static void crear_carros(String file) {
        // Creacion de objetos
        Carro objCarro_1 = new Carro("Verde", "Mazda", 4500);
        Carro objCarro_2 = new Carro("Gris", "Tesla", 6500);

        try {
            // Objeto para referenciar el fichero
            FileOutputStream archivo = new FileOutputStream(file);
            // Objeto de tipo stream para enviar objetos serializados
            ObjectOutputStream objStream = new ObjectOutputStream(archivo);
            // Envío de objetos al archivo
            objStream.writeObject(objCarro_1);
            objStream.writeObject(objCarro_2);
            // Cerramos el flujo de datos
            objStream.close();
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println("Error al guardar los datos");
        }
    }

    public static void cargar_carros(String file) {
        try {
            // Referenciamos el archivo
            FileInputStream fichero = new FileInputStream(file);
            // Creamos canal/flujo de datos para la entrada del contenido del fichero
            ObjectInputStream oStream = new ObjectInputStream(fichero);
            while (true) {
                Carro objCarro = (Carro) oStream.readObject();
                System.out.println("-------Carro---------");
                System.out.println("Color: " + objCarro.getColor());
                System.out.println("Fabricante: " + objCarro.getFabricante());
                System.out.println("Caballos de fuerza: " + objCarro.getCaballos_fuerza());
            }
            /*
             * Carro objCarro_1 = (Carro) oStream.readObject(); Carro objCarro_2 = (Carro)
             * oStream.readObject(); System.out.println("-------Carro 1---------");
             * System.out.println("Color: "+objCarro_1.getColor());
             * System.out.println("Fabricante: "+objCarro_1.getFabricante());
             * System.out.println("Caballos de fuerza: "+objCarro_1.getCaballos_fuerza());
             * System.out.println("-------Carro 2---------");
             * System.out.println("Color: "+objCarro_2.getColor());
             * System.out.println("Fabricante: "+objCarro_2.getFabricante());
             * System.out.println("Caballos de fuerza: "+objCarro_2.getCaballos_fuerza());
             *
            oStream.close();
            */
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
